import json
import os
from collections import OrderedDict

directory = "./checks_compiled"

monitoring_cores = list()
monitoring_cores.append("icinga2")

icinga2_itl_data = dict()
icinga2_itl_data["check_adaptec_raid"] = "adaptec-raid"
icinga2_itl_data["check_apache_status.pl"] = "apache-status"
icinga2_itl_data["check_apt"] = "apt"
icinga2_itl_data["check_breeze"] = "breeze"
icinga2_itl_data["sudo /usr/lib/nagios/plugins/check_btrfs"] = "btrfs"
icinga2_itl_data["check_by_ssh"] = "by_ssh"
icinga2_itl_data["check_ceph.py"] = "ceph"
icinga2_itl_data["check_memory"] = "check_memory"
icinga2_itl_data["check_clamd"] = "clamd"
icinga2_itl_data["check_cloudera_hdfs_files.py"] = "cloudera_hdfs_files"
icinga2_itl_data["check_cloudera_hdfs_space.py"] = "cloudera_hdfs_space"
icinga2_itl_data["check_cloudera_service_status.py"] = "cloudera_service_status"
# icinga2_itl_data['\N'] = 'cluster'
# icinga2_itl_data['\N'] = 'cluster-zone'
icinga2_itl_data["check_db2_health"] = "db2_health"
icinga2_itl_data["check_dhcp"] = "dhcp"
icinga2_itl_data["check_dig"] = "dig"
icinga2_itl_data["check_disk"] = "disk"
icinga2_itl_data["check_disk.exe"] = "disk-windows"
icinga2_itl_data["check_disk_smb"] = "disk_smb"
icinga2_itl_data["check_dns"] = "dns"
# icinga2_itl_data['\N'] = 'dummy'
icinga2_itl_data["check_elasticsearch"] = "elasticsearch"
icinga2_itl_data["check_esxi_hardware.py"] = "esxi_hardware"
# icinga2_itl_data['\N'] = 'exception'
icinga2_itl_data["sudo /usr/lib/nagios/plugins/check_fail2ban"] = "fail2ban"
icinga2_itl_data["check_file_age"] = "file_age"
icinga2_itl_data["check_flexlm"] = "flexlm"
icinga2_itl_data["check_fping $fping_address$ -4"] = "fping4"
icinga2_itl_data["check_fping $fping_address$ -6"] = "fping6"
icinga2_itl_data["check_ftp"] = "ftp"
icinga2_itl_data["check_game"] = "game"
icinga2_itl_data["sudo /usr/lib/nagios/plugins/check_glusterfs"] = "glusterfs"
icinga2_itl_data["check_graphite"] = "graphite"
icinga2_itl_data["check_haproxy"] = "haproxy"
icinga2_itl_data["check_haproxy_status"] = "haproxy_status"
icinga2_itl_data["check_hddtemp"] = "hddtemp"
icinga2_itl_data["check_ping"] = "hostalive"
icinga2_itl_data["check_ping -4"] = "hostalive4"
icinga2_itl_data["check_ping -6"] = "hostalive6"
icinga2_itl_data["check_hpasm"] = "hpasm"
icinga2_itl_data["check_hpjd"] = "hpjd"
icinga2_itl_data["check_http"] = "http"
# icinga2_itl_data['\N'] = 'icinga'
icinga2_itl_data["/usr/bin/icingacli businessprocess process check"] = (
    "icingacli-businessprocess"
)
icinga2_itl_data["/usr/bin/icingacli director health check"] = "icingacli-director"
icinga2_itl_data["/usr/bin/icingacli elasticsearch check"] = "icingacli-elasticsearch"
icinga2_itl_data["/usr/bin/icingacli x509 check host"] = "icingacli-x509"
icinga2_itl_data["check_icmp"] = "icmp"
# icinga2_itl_data['\N'] = 'ido'
icinga2_itl_data["check_iftraffic.pl"] = "iftraffic"
icinga2_itl_data["check_iftraffic64.pl"] = "iftraffic64"
icinga2_itl_data["check_imap"] = "imap"
icinga2_itl_data["check_interfaces"] = "interfaces"
icinga2_itl_data["check_interface_table_v3t"] = "interfacetable"
icinga2_itl_data["check_iostat"] = "iostat"
icinga2_itl_data["check_iostats"] = "iostats"
icinga2_itl_data["check_ping"] = "ipmi-alive"
icinga2_itl_data["check_ipmi_sensor"] = "ipmi-sensor"
icinga2_itl_data["check_jmx4perl"] = "jmx4perl"
icinga2_itl_data["check_kdc"] = "kdc"
icinga2_itl_data["check_ldap"] = "ldap"
icinga2_itl_data["check_lmsensors"] = "lmsensors"
icinga2_itl_data["check_load"] = "load"
icinga2_itl_data["check_load.exe"] = "load-windows"
icinga2_itl_data["check_logfiles"] = "logfiles"
icinga2_itl_data["check_logstash"] = "logstash"
icinga2_itl_data["check_lsi_raid"] = "lsi-raid"
icinga2_itl_data["check_lsyncd"] = "lsyncd"
icinga2_itl_data["/etc/icinga2/scripts/mail-host-notification.sh"] = (
    "mail-host-notification"
)
icinga2_itl_data["/etc/icinga2/scripts/mail-service-notification.sh"] = (
    "mail-service-notification"
)
icinga2_itl_data["check_mailq"] = "mailq"
icinga2_itl_data["check_mem.pl"] = "mem"
icinga2_itl_data["check_memcached"] = "memcached"
icinga2_itl_data["check_memory.exe"] = "memory-windows"
icinga2_itl_data["check_mongodb.py"] = "mongodb"
icinga2_itl_data["/etc/icinga2/scripts/monstack-mail-host-notification.py"] = (
    "monstack-mail-host-notification"
)
icinga2_itl_data["/etc/icinga2/scripts/monstack-mail-service-notification.py"] = (
    "monstack-mail-service-notification"
)
icinga2_itl_data["check_mssql_health"] = "mssql_health"
icinga2_itl_data["check_mysql"] = "mysql"
icinga2_itl_data["check_mysql_health"] = "mysql_health"
icinga2_itl_data["check_mysql_query"] = "mysql_query"
icinga2_itl_data["negate"] = "negate"
icinga2_itl_data["check_network.exe"] = "network-windows"
icinga2_itl_data["check_nginx_status.pl"] = "nginx_status"
icinga2_itl_data["check_nrpe"] = "nrpe"
icinga2_itl_data["check_nt"] = "nscp"
icinga2_itl_data["\\nscp.exe client"] = "nscp-local"
icinga2_itl_data["\\nscp.exe client"] = "nscp-local-counter"
icinga2_itl_data["\\nscp.exe client"] = "nscp-local-cpu"
icinga2_itl_data["\\nscp.exe client"] = "nscp-local-disk"
icinga2_itl_data["\\nscp.exe client"] = "nscp-local-memory"
icinga2_itl_data["\\nscp.exe client"] = "nscp-local-os-version"
icinga2_itl_data["\\nscp.exe client"] = "nscp-local-pagefile"
icinga2_itl_data["\\nscp.exe client"] = "nscp-local-process"
icinga2_itl_data["\\nscp.exe client"] = "nscp-local-service"
icinga2_itl_data["\\nscp.exe client"] = "nscp-local-tasksched"
icinga2_itl_data["\\nscp.exe client"] = "nscp-local-uptime"
icinga2_itl_data["\\nscp.exe client"] = "nscp-local-version"
icinga2_itl_data["check_nscp_api"] = "nscp_api"
icinga2_itl_data["check_ntp_peer"] = "ntp_peer"
icinga2_itl_data["check_ntp_time"] = "ntp_time"
icinga2_itl_data["check_nwc_health"] = "nwc_health"
icinga2_itl_data["check_openmanage"] = "openmanage"
icinga2_itl_data["check_oracle_health"] = "oracle_health"
# icinga2_itl_data['\N'] = 'passive'
icinga2_itl_data["check_perfmon.exe"] = "perfmon-windows"
icinga2_itl_data["check_pgsql"] = "pgsql"
icinga2_itl_data["check_phpfpm_status"] = "phpfpm_status"
icinga2_itl_data["check_ping"] = "ping"
icinga2_itl_data["check_ping.exe"] = "ping-windows"
icinga2_itl_data["check_ping -4"] = "ping4"
icinga2_itl_data["check_ping.exe -4"] = "ping4-windows"
icinga2_itl_data["check_ping -6"] = "ping6"
icinga2_itl_data["check_ping.exe -6"] = "ping6-windows"
icinga2_itl_data["check_pop"] = "pop"
icinga2_itl_data["check_postgres.pl"] = "postgres"
icinga2_itl_data["check_printer_health"] = "printer_health"
icinga2_itl_data["check_procs"] = "procs"
icinga2_itl_data["check_procs.exe"] = "procs-windows"
icinga2_itl_data["check_proxysql"] = "proxysql"
icinga2_itl_data["check_radius"] = "radius"
# icinga2_itl_data['\N'] = 'random'
icinga2_itl_data["check_rbl"] = "rbl"
icinga2_itl_data["check_redis.pl"] = "redis"
icinga2_itl_data["check_rpc"] = "rpc"
# icinga2_itl_data['\N'] = 'running_kernel'
icinga2_itl_data["check_sar_perf.py"] = "sar-perf"
icinga2_itl_data["check_service.exe"] = "service-windows"
icinga2_itl_data["check_simap"] = "simap"
# icinga2_itl_data['\N'] = 'sleep'
icinga2_itl_data["check_ide_smart"] = "smart"
icinga2_itl_data["check_smart_attributes"] = "smart-attributes"
icinga2_itl_data["check_smtp"] = "smtp"
icinga2_itl_data["check_snmp"] = "snmp"
icinga2_itl_data["check_snmp_env.pl"] = "snmp-env"
icinga2_itl_data["check_snmp_int.pl"] = "snmp-interface"
icinga2_itl_data["check_snmp_load.pl"] = "snmp-load"
icinga2_itl_data["check_snmp_mem.pl"] = "snmp-memory"
icinga2_itl_data["check_snmp_process.pl"] = "snmp-process"
icinga2_itl_data["check_snmp_win.pl"] = "snmp-service"
icinga2_itl_data["check_snmp_storage.pl"] = "snmp-storage"
icinga2_itl_data["check_snmp"] = "snmp-uptime"
icinga2_itl_data["check_snmp"] = "snmpv3"
icinga2_itl_data["check_spop"] = "spop"
icinga2_itl_data["check_squid"] = "squid"
icinga2_itl_data["check_ssh"] = "ssh"
icinga2_itl_data["check_tcp"] = "ssl"
icinga2_itl_data["check_ssl_cert"] = "ssl_cert"
icinga2_itl_data["check_ssmtp"] = "ssmtp"
icinga2_itl_data["check_swap"] = "swap"
icinga2_itl_data["check_swap.exe"] = "swap-windows"
icinga2_itl_data["check_tcp"] = "tcp"
icinga2_itl_data["check_udp -H $udp_address$ -p $udp_port$"] = "udp"
icinga2_itl_data["check_update.exe"] = "update-windows"
icinga2_itl_data["check_ups"] = "ups"
icinga2_itl_data["check_uptime"] = "uptime"
icinga2_itl_data["check_uptime.exe"] = "uptime-windows"
icinga2_itl_data["check_users"] = "users"
icinga2_itl_data["check_users.exe"] = "users-windows"
icinga2_itl_data["check_varnish"] = "varnish"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-dc-runtime-info"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-dc-runtime-issues"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-dc-runtime-listcluster"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-dc-runtime-listhost"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-dc-runtime-listvms"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-dc-runtime-status"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-dc-runtime-tools"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-dc-volumes"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-host-check"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-host-cpu"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-host-cpu-ready"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-host-cpu-usage"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-host-cpu-wait"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-host-io"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-host-io-aborted"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-host-io-device-latency"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-host-io-kernel-latency"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-host-io-queue-latency"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-host-io-read"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-host-io-read-latency"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-host-io-resets"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-host-io-total-latency"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-host-io-usage"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-host-io-write"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-host-io-write-latency"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-host-media"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-host-mem"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-host-mem-consumed"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-host-mem-memctl"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-host-mem-overhead"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-host-mem-swapused"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-host-mem-usage"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-host-net"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-host-net-nic"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-host-net-receive"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-host-net-send"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-host-net-usage"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-host-runtime"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-host-runtime-con"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-host-runtime-health"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-host-runtime-health-listsensors"
icinga2_itl_data["check_vmware_esx"] = (
    "vmware-esx-soap-host-runtime-health-nostoragestatus"
)
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-host-runtime-issues"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-host-runtime-listvms"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-host-runtime-status"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-host-runtime-storagehealth"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-host-runtime-temp"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-host-service"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-host-storage"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-host-storage-adapter"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-host-storage-lun"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-host-storage-path"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-host-uptime"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-host-volumes"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-vm-cpu"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-vm-cpu-ready"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-vm-cpu-usage"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-vm-cpu-wait"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-vm-io"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-vm-io-read"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-vm-io-usage"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-vm-io-write"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-vm-mem"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-vm-mem-consumed"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-vm-mem-memctl"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-vm-mem-usage"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-vm-net"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-vm-net-receive"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-vm-net-send"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-vm-net-usage"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-vm-runtime"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-vm-runtime-con"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-vm-runtime-consoleconnections"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-vm-runtime-gueststate"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-vm-runtime-issues"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-vm-runtime-powerstate"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-vm-runtime-status"
icinga2_itl_data["check_vmware_esx"] = "vmware-esx-soap-vm-runtime-tools"
icinga2_itl_data["check_webinject"] = "webinject"
icinga2_itl_data["check_yum"] = "yum"

print("from django.db import models")
print()
print()
print("class AbstractCheckCommand(models.Model):")
print("    computer_id_ici = models.IntegerField(blank=False, null=False)")
print("    check_enabled = models.BooleanField(default=True)")
print("    notify_enabled = models.BooleanField(default=True)")
print("    run_on_endpoint = models.BooleanField(default=True)")
print("    check_interval = models.IntegerField(default=30)")
print("    check_timeout = models.IntegerField(default=30)")
print("    check_retry_interval = models.IntegerField(default=30)")
print("    check_attempts = models.IntegerField(default=3)")
print()
print("    class Meta:")
print("        abstract = True")
print()

checks = OrderedDict(json.load(open("generate_django_orm_models.json", "r")))

json_file = open("generate_django_orm_models.json", "w+")

for filename in os.listdir(directory):
    if filename.startswith("check_"):
        if filename not in checks:
            checks[filename] = dict()

        normalized_check_command = filename.lower().replace("-", "_")

        if "normalized_check_command" not in checks[filename]:
            checks[filename]["normalized_check_command"] = normalized_check_command

        if "normalized_check_command_class" not in checks[filename]:
            checks[filename]["normalized_check_command_class"] = str()

            for split_name in normalized_check_command.split("_"):
                split_name = list(split_name)
                split_name[0] = split_name[0].upper()
                checks[filename]["normalized_check_command_class"] += "".join(
                    split_name
                )

        if "matrix" not in checks[filename]:
            checks[filename]["matrix"] = dict()

        for monitoring_core in monitoring_cores:
            if monitoring_core not in checks[filename]["matrix"]:
                checks[filename]["matrix"][monitoring_core] = "dummy"

        if "managed_by_core" not in checks[filename]:
            checks[filename]["managed_by_core"] = dict()

        if "qa_passed" not in checks[filename]:
            checks[filename]["qa_passed"] = "no"

        for monitoring_core in monitoring_cores:
            if monitoring_core not in checks[filename]["managed_by_core"]:
                if checks[filename]["matrix"][monitoring_core] == "dummy":
                    checks[filename]["managed_by_core"][monitoring_core] = "no"
                else:
                    checks[filename]["managed_by_core"][monitoring_core] = "yes"

        if "arguments" not in checks[filename]:
            checks[filename]["arguments"] = dict()

        for argument, argument_data in checks[filename]["arguments"].items():
            if "default" not in argument_data:
                argument_data["default"] = None
            if "required" not in argument_data:
                argument_data["required"] = False

        # icinga2 automation
        if checks[filename]["matrix"]["icinga2"] == "dummy":
            checks[filename]["matrix"]["icinga2"] = checks[filename][
                "normalized_check_command"
            ].replace("_", "-")[6:]

        if checks[filename] in icinga2_itl_data.items():
            checks[filename]["matrix"]["icinga2"] = icinga2_itl_data[checks[filename]]
            checks[filename]["managed_by_core"]["icinga2"] = "yes"

checks = OrderedDict(
    sorted(checks.items(), key=lambda x: x[1]["normalized_check_command"])
)

for filename, check_data in checks.items():
    if check_data["qa_passed"] == "no":
        continue

    print()
    print(
        "class "
        + check_data["normalized_check_command_class"]
        + "(AbstractCheckCommand):"
    )

    for argument_name, argument_data in check_data["arguments"].items():
        argument_name_model = argument_data["value"].replace("-", "_")

        # argument_name_model = argument_name.replace('-', '_')
        # argument_name_model = list('param_' + argument_name_model)
        # argument_name_model_tmp = list()
        #
        # last_char = str()
        #
        # for current_char in argument_name_model:
        #     if not (current_char == '_' and last_char == '_'):
        #         argument_name_model_tmp.append(current_char)
        #         if current_char.isupper():
        #             argument_name_model_tmp.append('1')
        #     last_char = current_char
        #
        # argument_name_model = "".join(argument_name_model_tmp).lower()

        if argument_data["required"]:
            null_text = "null=False"
        else:
            null_text = "null=True"

        print("    " + argument_name_model + " = models.CharField(")
        print("        max_length=256,")
        print("        " + null_text + ",")
        print("        blank=False,")
        print(
            "        help_text='"
            + argument_data["description"].replace("'", "\\")
            + "',"
        )
        print("        verbose_name='" + argument_name + "',")

        if argument_data["default"] is not None:
            print("        default='" + argument_data["default"] + "',")

        print("    )")

    print()
    print("    def mon_core_check_name(self):")
    print("        matrix = {")

    for monitoring_core, matrix_row in sorted(check_data["matrix"].items()):
        print("            '" + monitoring_core + "': '" + matrix_row + "',")

    print("        }")
    print("        return matrix")

    print()
    print("    def mon_core_check_command(self):")
    print("        return '" + filename + "'")
    print()
    print("    class Meta:")
    print("        db_table = '" + check_data["normalized_check_command"].lower() + "'")
    print()

json.dump(checks, json_file, sort_keys=True, indent=4)

for filename, check_data in checks.items():
    if check_data["managed_by_core"]["icinga2"] == "no":
        with open(
            "mon_core/icinga2/itl/" + checks[filename]["matrix"]["icinga2"] + ".conf",
            "w",
        ) as conf_file:
            conf_file.write(
                "/* Kevin Olbrich / Dolphin IT | (c) 2020 Dolphin IT-Systeme e.K. | LGPLv3 */\n"
            )
            conf_file.write("\n")
            conf_file.write(
                'template CheckCommand "'
                + checks[filename]["matrix"]["icinga2"]
                + '" {\n'
            )
            conf_file.write('	command = [ PluginDir + "/' + filename + '" ]\n')
            conf_file.write("\n")
            conf_file.write("	arguments = {\n")

            for argument_name, argument_data in check_data["arguments"].items():
                conf_file.write('		"' + argument_name + '" = {\n')
                conf_file.write(
                    '			value = "$' + argument_data["value"].lower() + '$"\n'
                )
                conf_file.write(
                    '			description = "'
                    + argument_data["description"].replace("'", "\\")
                    + '"\n'
                )
                conf_file.write("		}\n")

            conf_file.write("	}\n")
            conf_file.write("}\n")
