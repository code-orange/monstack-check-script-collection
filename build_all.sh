#!/bin/bash
set -e
set -x

#shopt -s extglob

# clean binaries
cd checks_compiled
git checkout master
git pull
#rm -v !("LICENSE"|"README.md") 
cd ..

# build all checks
cd checks/
for chk_name in */; do
    cd $chk_name
    cd src/
    git pull || true
    cd ..
    sh build.sh || true
    cd ../../
	# commit binaries
	cd checks_compiled
	git add -A
	git update-index --chmod=+x *
	git commit -m "update check binaries" || true
	git push || true
	cd ../
	cd checks/
done
cd ..

# add pseudo-upstream commands
cd checks_compiled
touch ./check_adaptec_raid
touch ./check_apache_status
touch ./check_apt
touch ./check_breeze
touch ./check_btrfs
touch ./check_by_ssh
touch ./check_ceph
touch ./check_clamd
touch ./check_cloudera_hdfs_files
touch ./check_cloudera_hdfs_space
touch ./check_cloudera_service_status
touch ./check_db2_health
touch ./check_dhcp
touch ./check_dig
touch ./check_disk
touch ./check_disk_smb
touch ./check_dns
touch ./check_elasticsearch
touch ./check_esxi_hardware
touch ./check_fail2ban
touch ./check_file_age
touch ./check_flexlm
touch ./check_ftp
touch ./check_game
touch ./check_glusterfs
touch ./check_graphite
touch ./check_haproxy
touch ./check_haproxy_status
touch ./check_hddtemp
touch ./check_hpasm
touch ./check_hpjd
touch ./check_http
touch ./check_icmp
touch ./check_ide_smart
touch ./check_iftraffic
touch ./check_iftraffic64
touch ./check_imap
touch ./check_interfaces
touch ./check_interface_table_v3t
touch ./check_iostat
touch ./check_iostats
touch ./check_ipmi_sensor
touch ./check_jmx4perl
touch ./check_kdc
touch ./check_ldap
touch ./check_lmsensors
touch ./check_load
touch ./check_logfiles
touch ./check_logstash
touch ./check_lsi_raid
touch ./check_lsyncd
touch ./check_mailq
touch ./check_mem
touch ./check_memcached
touch ./check_mongodb
touch ./check_mssql_health
touch ./check_mysql
touch ./check_mysql_health
touch ./check_mysql_query
touch ./check_nginx_status
touch ./check_nrpe
touch ./check_nscp_api
touch ./check_nt
touch ./check_ntp_peer
touch ./check_ntp_time
touch ./check_nwc_health
touch ./check_openmanage
touch ./check_oracle_health
touch ./check_pgsql
touch ./check_phpfpm_status
touch ./check_ping
touch ./check_pop
touch ./check_postgres
touch ./check_printer_health
touch ./check_procs
touch ./check_proxysql
touch ./check_rbl
touch ./check_redis
touch ./check_rpc
touch ./check_sar_perf
touch ./check_simap
touch ./check_smart_attributes
touch ./check_smtp
touch ./check_snmp
touch ./check_snmp_env
touch ./check_snmp_int
touch ./check_snmp_load
touch ./check_snmp_mem
touch ./check_snmp_process
touch ./check_snmp_storage
touch ./check_snmp_win
touch ./check_spop
touch ./check_squid
touch ./check_ssh
touch ./check_ssl_cert
touch ./check_ssmtp
touch ./check_swap
touch ./check_tcp
touch ./check_ups
touch ./check_uptime
touch ./check_users
touch ./check_varnish
touch ./check_vmware_esx
touch ./check_webinject
touch ./check_yum
git add -A
git update-index --chmod=+x *
git commit -m "update check binaries" || true
git push || true
cd ../

# build database models
cd mon_core/icinga2/itl
git rm *.conf || true
cd ../../../

python3 generate_django_orm_models.py > django_monstack_check_commands/django_monstack_check_commands/models.py
cd django_monstack_check_commands/
git add django_monstack_check_commands/models.py
git commit -m "update models" || true
git push || true
cd ..

cd mon_core/icinga2/itl
git add *.conf || true
git commit -m "update itl" || true
git push || true
cd ../../../

git add generate_django_orm_models.json || true
git commit -m "update generate_django_orm_models.json" || true
