#!/bin/bash
set -e
set -x

git config credential.helper store

git submodule foreach -q --recursive 'git checkout $(git config -f $toplevel/.gitmodules submodule.$name.branch || echo master)'
git submodule update --jobs=8 --init --remote --merge --force
git submodule foreach 'git commit -a -m "update submodules" || true'
git submodule foreach 'git push --recurse-submodules=on-demand || true'
git commit -a -m "update submodules" || true
git push origin master || true
