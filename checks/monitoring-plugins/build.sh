#!/bin/bash
set -e

# compile
cd src/
./tools/setup
./configure --enable-static --enable-perl-modules
make

# copy binary
cp plugins/check_apt ../../../checks_compiled/
cp plugins/check_by_ssh ../../../checks_compiled/
cp plugins/check_cluster ../../../checks_compiled/
cp plugins/check_dig ../../../checks_compiled/
cp plugins/check_disk ../../../checks_compiled/
cp plugins/check_dns ../../../checks_compiled/
cp plugins/check_dummy ../../../checks_compiled/
cp plugins/check_hpjd ../../../checks_compiled/
cp plugins/check_http ../../../checks_compiled/
cp plugins/check_ide_smart ../../../checks_compiled/
cp plugins/check_ldap ../../../checks_compiled/
cp plugins/check_load ../../../checks_compiled/
cp plugins/check_mrtg ../../../checks_compiled/
cp plugins/check_mrtgtraf ../../../checks_compiled/
cp plugins/check_mysql ../../../checks_compiled/
cp plugins/check_mysql_query ../../../checks_compiled/
cp plugins/check_nagios ../../../checks_compiled/
cp plugins/check_nt ../../../checks_compiled/
cp plugins/check_ntp ../../../checks_compiled/
cp plugins/check_ntp_peer ../../../checks_compiled/
cp plugins/check_ntp_time ../../../checks_compiled/
cp plugins/check_nwstat ../../../checks_compiled/
cp plugins/check_overcr ../../../checks_compiled/
cp plugins/check_pgsql ../../../checks_compiled/
cp plugins/check_ping ../../../checks_compiled/
cp plugins/check_procs ../../../checks_compiled/
cp plugins/check_real ../../../checks_compiled/
cp plugins/check_smtp ../../../checks_compiled/
cp plugins/check_snmp ../../../checks_compiled/
cp plugins/check_ssh ../../../checks_compiled/
cp plugins/check_swap ../../../checks_compiled/
cp plugins/check_tcp ../../../checks_compiled/
cp plugins/check_time ../../../checks_compiled/
cp plugins/check_ups ../../../checks_compiled/
cp plugins/check_users ../../../checks_compiled/
