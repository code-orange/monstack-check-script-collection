#!/bin/bash
set -e

# compile
cd src/

# copy binary
cp check_interface_table_v2.pl ../../../checks_compiled/check_interface_table_v2
