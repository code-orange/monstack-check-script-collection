#!/bin/bash
set -e

# compile
cd src/

# copy binary
cp check_emc_centera.pl ../../../checks_compiled/check_emc_centera
cp check_emc_clariion.pl ../../../checks_compiled/check_emc_clariion
