#!/bin/bash
set -e

# compile
cd src/

# copy binary
cp check_ipmi_sensor ../../../checks_compiled/check_ipmi_sensor
