#!/bin/bash
set -e

# compile
cd src/

# copy binary
cp check_nagios_checkresult.py ../../../checks_compiled/check_nagios_checkresult
