#!/bin/bash
set -e

# compile
cd src/

# copy binary
cp Dell\ iDRAC/check_idrac_ipmi ../../../checks_compiled/check_idrac_ipmi
cp Dell\ iDRAC/check_ipmi ../../../checks_compiled/check_ipmi
cp Dell\ iDRAC/check_ipmi_2_0.pl ../../../checks_compiled/check_ipmi_2_0

cp Juniper/check_juniper_mx ../../../checks_compiled/check_juniper_mx
cp Juniper/check_juniper_srx ../../../checks_compiled/check_juniper_srx
cp Juniper/check_juniper_srx_chassis_cluster ../../../checks_compiled/check_juniper_srx_chassis_cluster
cp Juniper/check_netscreen_session ../../../checks_compiled/check_netscreen_session

cp nfdump/check_nfdump_alert ../../../checks_compiled/check_nfdump_alert
cp nfdump/check_nfdump_report ../../../checks_compiled/check_nfdump_report

cp OpenSIPS/check_opensips_statistics ../../../checks_compiled/check_opensips_statistics

cp RTPProxy/check_rtpproxy.pl ../../../checks_compiled/check_rtpproxy
cp RTPProxy/check_media_relay ../../../checks_compiled/check_media_relay
cp RTPProxy/check_rtpproxy_calls ../../../checks_compiled/check_rtpproxy_calls
cp RTPProxy/check_rtpproxy_media ../../../checks_compiled/check_rtpproxy_media
cp RTPProxy/check_rtpproxy2 ../../../checks_compiled/check_rtpproxy2

cp SIP/check_sip_options ../../../checks_compiled/check_sip_options
cp SIP/check_sip2 ../../../checks_compiled/check_sip2
cp SIP/check_sipsak ../../../checks_compiled/check_sipsak
cp SIP/check_sipsak_statistics ../../../checks_compiled/check_sipsak_statistics
