#!/bin/bash
set -e

# compile
cd src/

# copy binary
cp check_casperjs.pl ../../../checks_compiled/check_casperjs
