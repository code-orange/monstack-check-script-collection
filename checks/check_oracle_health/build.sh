#!/bin/bash
set -e

# compile
cd src/
autoreconf -i
./configure --enable-static --disable-maintainer-mode --prefix=/usr --with-nagios-user=nagios --with-nagios-group=nagios
make

# copy binary
cp plugins-scripts/check_oracle_health ../../../checks_compiled/check_oracle_health
