#!/bin/bash
set -e

# compile
cd src/

# copy binary
cp check_knuerr_rms.pl ../../../checks_compiled/check_knuerr_rms
