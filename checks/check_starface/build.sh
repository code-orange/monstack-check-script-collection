#!/bin/bash
set -e

# compile
cd src/

# copy binary
cp check_starface.pl ../../../checks_compiled/check_starface
cp check_starface_snmp.pl ../../../checks_compiled/check_starface_snmp
