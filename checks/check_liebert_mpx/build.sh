#!/bin/bash
set -e

# compile
cd src/

# copy binary
cp check_liebert_mpx.pl ../../../checks_compiled/check_liebert_mpx
