#!/bin/bash
set -e

# compile
cd src/

# copy binary
cp check_sip_call.pl ../../../checks_compiled/check_sip_call
