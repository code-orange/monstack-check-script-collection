#!/bin/bash
set -e

# compile
cd src/

# copy binary
cp check_gpu_sensor ../../../checks_compiled/check_gpu_sensor
