#!/bin/bash
set -e

# compile
cd src/

# copy binary
cp plugins-scripts/check_logfiles ../../../checks_compiled/check_logfiles
