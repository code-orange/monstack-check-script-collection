#!/bin/bash
set -e

# compile
cd src/

# copy binary
cp check_ica_servers.pl ../../../checks_compiled/check_ica_servers
