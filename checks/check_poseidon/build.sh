#!/bin/bash
set -e

# compile
cd src/

# copy binary
cp check_poseidon.pl ../../../checks_compiled/check_poseidon
