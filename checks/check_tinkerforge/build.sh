#!/bin/bash
set -e

# compile
cd src/

# copy binary
cp check_tinkerforge.py ../../../checks_compiled/check_tinkerforge
