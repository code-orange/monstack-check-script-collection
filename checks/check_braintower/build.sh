#!/bin/bash
set -e

# compile
cd src/

# copy binary
cp check_braintower ../../../checks_compiled/check_braintower
