#!/bin/bash
set -e

# compile
cd src/
#perl Makefile.PL
#make

# copy binary
cp libexec/check_xmpp ../../../checks_compiled/
