#!/bin/bash
set -e

# compile
cd src/

# copy binary
cp check_knuerr_pdu.pl ../../../checks_compiled/check_knuerr_pdu
