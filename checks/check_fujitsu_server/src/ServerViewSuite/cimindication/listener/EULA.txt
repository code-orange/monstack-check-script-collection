Fujitsu Limited
Software License Agreement for End Users

1. Subject of this Agreement
1.1. For the purposes of this agreement “Software” shall mean the software with 
the object code, the version and the specification indicated in the software 
product data sheet of Fujitsu Limited or one of its subsidiaries (collectively 
“Fujitsu”).

The Software consists of machine-readable instructions and/or printed 
documentation and related licensed materials (“Documentation”).

1.2. Please read this agreement carefully before you use the Software. If you 
do not agree with the license terms in this agreement, you are not permitted to 
use the Software and must immediately return all copies of the Software and all 
accompanying items to the Licensor/Sublicensor (either Fujitsu or the reseller 
who supplied you with the Software) with proof of purchase for a full refund.

1.3. Any use of the Software requires the proper payment of the applicable 
license fees. By using the Software you agree to be bound by the terms of this 
agreement.

1.4. Fujitsu reserves the right to implement at any time in the future an 
additional software license key and/or license certificates as countermeasures 
against software piracy. 

1.5. Software components from third-party software suppliers which are part of 
the scope of the delivery are subject to separate license agreements that are 
included with the Software or that are transmitted by Fujitsu upon request.

2. End User License
2.1. Fujitsu grants you a non-exclusive and non-transferable license to use the 
Software on the number of workstations for which you have purchased licenses. 
Unless you purchase additional licenses, you are not permitted to operate the 
Software on more than the maximum number of licensed workstations or on 
hardware that exceeds the specified type.

You are permitted to make a backup copy of the Software for archiving purposes, 
provided you properly mark the copy or partial copy of the Software with the 
copyright notice and any other ownership information.

2.2. You are not permitted to copy, modify or distribute the Software. 
Furthermore, you are not permitted to re-compile, re-engineer, convert, revise, 
compile or modify the Software. You may not sub-license, without selling the 
related hardware, assign, rent, lease or transfer the Software except as 
expressly permitted by this agreement or due to mandatory legal regulations.

2.3. If you acquired the Software as a program upgrade, your license for the 
use of the old software version ends automatically with the installation of the 
upgrade version of the Software. If parts of the old software version are not 
replaced by the upgrade version, the license for the old version continues to 
be effective until the remnants of the old software version are also replaced 
or deactivated or shut down in any other way.

2.4. Unless specified otherwise in the respective software data sheet of 
Fujitsu, the license for a software version or release does not give you any 
rights to new releases (updates), new versions (upgrades) or technical support 
services for the Software. Supplemental software support contracts and 
maintenance services, including or excluding new releases and new versions and 
additional technical support services, can be purchased separately either from 
Fujitsu directly or from authorized software resellers.

2.5. In case expressly permitted under the Documentation, you may embed the 
whole or part of the Software into your software program in accordance with the 
terms and conditions described in the Documentation.

3. Downloading
For Software supplied by Fujitsu over a network or a similar distribution path, 
the following additional conditions shall apply: 
All products supplied for downloading by Fujitsu are selected, made available 
and — if supplied by third parties — provided without modification. However, 
you are fully responsible for ensuring the most current version and usability 
of downloadable material for your own purposes and on your own system. You 
download Software at your own risk. Fujitsu will not accept any liability, 
particularly not for transmission errors or problems that arise during the 
downloading process (line failures, connection interruptions, server failures, 
data corruption, etc.).

The website of Fujitsu is operated and administered only for those countries in 
which Fujitsu has one or more offices. Fujitsu accepts no responsibility that 
Software and/or documentation can or may be downloaded from a Fujitsu website 
also in locations other than the countries mentioned above. If you access a 
website of Fujitsu from abroad, you are fully responsible for complying with 
any local regulations. Fujitsu expressly prohibits the downloading of Software 
and/or documentation from a Fujitsu website in countries where such downloading 
is considered illegal.

4. Copyright
All rights and licenses, unless they are expressly granted to you in this 
license terms, as well as all property and usage rights related to the Software 
(including parts of the Software) remain fully with Fujitsu and/or its 
third-party licensors.

The license terms do not authorize you to use the brands, logos or trademarks 
of Fujitsu or its third-party licensors, nor are you permitted to use any other 
brands which are deceptively similar to the brands, logos or trademarks of 
Fujitsu. Each and any use of brands, logos or trademarks with respect to the 
Software or Fujitsu requires the express consent of Fujitsu.

5. Licensor’s warranty and liability disclaimer, if Software is sold and 
delivered by Reseller
If you acquire the Software directly from an authorized reseller (called 
“Reseller”), the right to install and use the Software may be subject to 
additional software license conditions agreed upon between you as the licensee 
and the respective reseller.

In all cases of an authorized software resale, the software is sublicensed and 
made available to the licensee directly by the Reseller. In such cases, Fujitsu 
is not a contractual party of the software license agreement between you, as 
licensee and the Reseller, as far as the procurement of the software licenses 
are concerned. Legal claims in connection with the software licensing can 
therefore be asserted only on the basis of the agreements with the Reseller. 
Under no circumstances, however, will the respective scope of the license for 
the licensee exceed the scope of the license agreements as specified in 
sections 1, 2, 3 and 4 of this agreement. 

Subject to mandatory legal regulations, particularly those governing liability 
and/or warranties, which cannot be excluded in connection with end user license 
agreement regulations and with reference to the licensee’s claims against the 
Reseller, Fujitsu disclaims all warranties for the Software in this agreement. 
For the same reason, Fujitsu disclaims any and all liability/claims for any 
violations of third parties’ rights as well as any implied warranties for the 
software’s marketability and its suitability for a particular purpose. This 
disclaimer of liability does not apply in cases of willful or malicious 
behavior by Fujitsu.

In this End User License Agreement, Fujitsu grants no warranties of any kind, 
either express or implied.

6. Disclaimer of liability with respect to shareware, freeware and/or open 
source software components
6.1. The Software may contain freeware or shareware which Fujitsu received from 
a third party. Fujitsu paid no license fees for the use of this freeware or 
shareware. Accordingly, the licensee is not charged any license fees for the 
use of the freeware or shareware. You recognize and accept that Fujitsu 
therefore grants no warranties with respect to such freeware or shareware 
components and does not assume any liability in connection with the ownership, 
the distribution and/or the use of the respective freeware or shareware.

6.2. The Software may also contain open source software components that were 
developed according to the “open source model” and which are distributed 
exclusively on the basis of the GPL (General Public License: 
http://www.gnu.org/copyleft/gpl.html) terms and conditions or other standard 
open source standard license terms and conditions applicable to the respective 
open source components at the time of their dissemination. You recognize and 
accept that the licensing of such open source software components is governed 
exclusively by the above-mentioned GPL terms or by the conditions which are 
otherwise included with the open source software components. Fujitsu receives 
neither license fees nor any other compensation for the delivered open source 
software components. As far as Fujitsu or a third party receives any 
compensation in connection with open source software components, it is received 
exclusively for additional delivery items and/or services.

Because of the special nature of the development and distribution of open 
source software components, Fujitsu assumes no express or implied liability for 
such components and excludes any kind of warranty for such open source software 
components, particularly in connection with missing specifications, lack of 
functionality, programming errors or any other malfunctions.

7. General limitations of liability
7.1. Neither Fujitsu nor its suppliers are liable for any consequential or 
indirect damages, including damages arising as a result of or in connection 
with an operational interruption, lost profits or sales, lost data, or costs of 
capital. Fujitsu and its suppliers will not be liable for additional ancillary 
or consequential costs or for any other losses, costs or expenses of any kind 
which arise as a result of the holding, sale, use or impossibility of use of 
the Software, independent of whether such claims are asserted due to warranty 
rights, contracts, tort or any other legal theory.

7.2. The liability of Fujitsu for direct damage caused as a result of a 
contract violation and/or other action or lapse on the part of Fujitsu which 
have not been excluded or cannot be completely excluded due to mandatory law 
are limited to no more than $300,000.00. Any and all other liabilities for 
direct damage are excluded. Damage caused by Fujitsu as a result of slight 
negligence are excluded to the extent permitted by applicable legal 
regulations. 

7.3. Limitations and exclusions of liability resulting from this agreement do 
not apply to damage where Fujitsu carries compulsory liability according to 
applicable laws and where such liability cannot be limited to a maximum amount 
(for example, liability for bodily damage; product liability or fraudulently 
incorrect information).

7.4. You acknowledge and agree that the Software is designed, developed and 
manufactured as contemplated for general use, including without limitation, 
general office use, personal use, household use, and ordinary industrial use, 
but is not designed, developed and manufactured as contemplated for use 
accompanying fatal risks or dangers that, unless extremely high safety is 
secured, could lead directly to death, personal injury, severe physical damage 
or other loss (hereinafter “High Safety Required Use”), including without 
limitation, nuclear reaction control in nuclear facility, aircraft flight 
control, air traffic control, mass transport control, medical life support 
system, missile launch control in weapon system. You shall not use the Software 
without securing the sufficient safety required for the High Safety Required 
Use. In addition, Fujitsu shall not be liable against you and/or any third 
party for any claims or damages arising in connection with the High Safety 
Required Use of the Software

8. Export controls
Due to its components as well as the nature or purpose of these components, the 
export of the Software and/or its accompanying documents may be subject to 
official or regulatory approval. In cases where the Software is intended for 
export, you are obliged to get all approvals and authorizations required to 
comply with all relevant export regulations.  

The Software may not be exported if there is reason to assume that the Software 
will be used in connection with nuclear, chemical or biological weapons or for 
missile technology. Furthermore, you may not deliver the Software — or have it 
delivered indirectly — to such companies or persons who are listed in the 
applicable U.S. export regulations (particularly the Table of Denial 
Orders/U.S. Denied Persons Lists (DPL) or in the E.U. export regulations 
(particularly the EU Terrorist List) or in the applicable warnings issued by 
Japan export authorities or any other competent authorities in any country. 

Under no circumstances is Fujitsu obligated to deliver software, patches, 
updates or upgrades, to provide software for download or to fulfill any other 
contractual commitments if this would be a violation of the applicable export 
regulations of Japan, the European Union, the United States of America or of 
any other countries.

If you export or re-export the Software or a copy of it, this may be a 
violation of applicable export laws and a severe violation of the terms of this 
agreement.

9. Miscellaneous
9.1. If any term or condition in this agreement or any other contract that is 
subject to the terms and conditions of this agreement turns out to be invalid 
or unenforceable (partly or in full), the validity of all other terms and 
conditions remains unaffected, unless complying with the remaining terms and 
conditions would represent an unreasonable hardship for either contract party, 
even with the application of applicable legal regulations to close the legal 
gap.

9.2. If you/ the licensee do not pay the license fees due and/or if the 
licensee does not comply with essential terms and conditions of this license 
agreement, Fujitsu reserves the right to cancel the license. In case of such 
cancellation, you must immediately return any and all copies of the software in 
your possession and confirm the complete return [of the software copies] or the 
destruction of these copies in writing.

9.3. Neither you nor Fujitsu is responsible or liable for the respective 
party’s non-compliance with its obligations if the reason for such 
non-compliance is outside the party’s control due to force majeure.

9.4. Any and all modifications and/or amendments to these license terms and 
conditions are only valid if they are made in writing.

9.5. Fujitsu may, at its expense, appoint its own personnel or an independent 
third party to audit the numbers of copies and installations as well as usage 
of the Software in use by you. Any such audit shall be conducted upon thirty 
(30) days prior notice, during regular business hours on your offices and shall 
not unreasonably interfere with your business activities.

10. Applicable law
10.1. These license terms and conditions are governed by the laws of Japan.

10.2. In the event that provisions of clause 10.1 are unenforceable, these 
license terms and conditions shall be governed by the laws of the country in 
which you acquire the Software, with the following exceptions: 1) In Australia, 
the terms and conditions of this license are governed by the laws of the state 
or sovereign territory in which the business contract is being concluded; 2) in 
Albania, Armenia, Belarus, Bosnia-Herzegovina, Bulgaria, Croatia, the Czech 
Republic, Georgia, Hungary, Kazakhstan, Kirgizia, the former Yugoslavian 
Republic of Macedonia (FYROM), Moldavia, Poland, Romania, Russia, Slovakia, 
Slovenia, the Ukraine and the Federal Republic of Yugoslavia, the terms and 
conditions of this license are governed by the laws of the Federal Republic of 
Germany; 3) in the United Kingdom [Great Britain], all disputes with respect to 
these license terms and conditions are governed by English law, and English 
courts have exclusive jurisdiction; 4) in Canada, the terms and conditions of 
this license are governed by the laws of the Province of Ontario; 5) in the 
United States of America and in Puerto Rico as well as in the People’s Republic 
of China the terms and conditions of this license are governed by the laws of 
the U.S. State of New York.












