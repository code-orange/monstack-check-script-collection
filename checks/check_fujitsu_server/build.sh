#!/bin/bash
set -e

# compile
cd src/

# copy binary
cp ServerViewSuite/nagios/plugin/check_fujitsu_server.pl ../../../checks_compiled/check_fujitsu_server
cp ServerViewSuite/nagios/plugin/check_fujitsu_server_CIM.pl ../../../checks_compiled/check_fujitsu_server_CIM
cp ServerViewSuite/nagios/plugin/check_fujitsu_server_REST.pl ../../../checks_compiled/check_fujitsu_server_REST
cp ServerViewSuite/nagios/plugin/discover_fujitsu_server.pl ../../../checks_compiled/discover_fujitsu_server
cp ServerViewSuite/nagios/plugin/fujitsu_server_wsman.pl ../../../checks_compiled/fujitsu_server_wsman
cp ServerViewSuite/nagios/plugin/inventory_fujitsu_server.pl ../../../checks_compiled/inventory_fujitsu_server
cp ServerViewSuite/nagios/plugin/tool_fujitsu_server.pl ../../../checks_compiled/tool_fujitsu_server
cp ServerViewSuite/nagios/plugin/tool_fujitsu_server_CIM.pl ../../../checks_compiled/tool_fujitsu_server_CIM
cp ServerViewSuite/nagios/plugin/tool_fujitsu_server_REST.pl ../../../checks_compiled/tool_fujitsu_server_REST
cp ServerViewSuite/nagios/plugin/updmanag_fujitsu_server_CIM.pl ../../../checks_compiled/updmanag_fujitsu_server_CIM
cp ServerViewSuite/nagios/plugin/updmanag_fujitsu_server_REST.pl ../../../checks_compiled/updmanag_fujitsu_server_REST
