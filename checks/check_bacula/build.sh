#!/bin/bash
set -e

# compile
cd src/

# copy binary
cp check_bacula.pl ../../../checks_compiled/check_bacula
cp count_backup.pl ../../../checks_compiled/count_backup
