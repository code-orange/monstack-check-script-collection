#!/bin/bash
set -e

# compile
cd src/

# copy binary
cp check_fujitsu_primergy.pl ../../../checks_compiled/check_fujitsu_primergy
