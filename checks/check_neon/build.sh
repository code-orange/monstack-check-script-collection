#!/bin/bash
set -e

# compile
cd src/

# copy binary
cp check_neon.py ../../../checks_compiled/check_neon
