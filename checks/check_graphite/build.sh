#!/bin/bash
set -e

# compile
cd src/

# copy binary
cp check_graphite.py ../../../checks_compiled/check_graphite
