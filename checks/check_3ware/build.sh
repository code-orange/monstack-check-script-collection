#!/bin/bash
set -e

# compile
cd src/

# copy binary
cp check_3ware.pl ../../../checks_compiled/check_3ware
cp check_3ware_2 ../../../checks_compiled/check_3ware_2
cp check_dpti.pl ../../../checks_compiled/check_dpti
