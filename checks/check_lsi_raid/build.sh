#!/bin/bash
set -e

# compile
cd src/

# copy binary
cp check_lsi_raid ../../../checks_compiled/check_lsi_raid
