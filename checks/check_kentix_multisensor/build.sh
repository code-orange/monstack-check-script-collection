#!/bin/bash
set -e

# compile
cd src/

# copy binary
cp check_kentix_multisensor ../../../checks_compiled/check_kentix_multisensor
