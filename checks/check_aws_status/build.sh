#!/bin/bash
set -e

# compile
cd src/

# copy binary
cp check_aws_status.rb ../../../checks_compiled/check_aws_status
