#!/bin/bash
set -e

# compile
cd src/

make

# copy binary
cp snmp_bulkget ../../../checks_compiled/snmp_bulkget
