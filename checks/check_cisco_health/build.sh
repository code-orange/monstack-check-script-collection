#!/bin/bash
set -e

# compile
cd src/

make

# copy binary
cp check_cisco_health ../../../checks_compiled/check_cisco_health
