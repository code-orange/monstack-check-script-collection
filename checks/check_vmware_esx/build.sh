#!/bin/bash
set -e

# compile
cd src/

# copy binary
cp check_vmware_esx.pl ../../../checks_compiled/check_vmware_esx
