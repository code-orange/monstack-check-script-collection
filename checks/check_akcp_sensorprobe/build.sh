#!/bin/bash
set -e

# compile
cd src/

# copy binary
cp check_akcp_sensorprobe.pl ../../../checks_compiled/check_akcp_sensorprobe
