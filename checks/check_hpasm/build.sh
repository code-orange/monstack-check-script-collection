#!/bin/bash
set -e

# compile
cd src/

# copy binary
cp plugins-scripts/check_hpasm ../../../checks_compiled/check_hpasm
