#!/bin/bash
set -e

# compile
cd src/

# copy binary
cp src/check_ceph_df ../../../checks_compiled/
cp src/check_ceph_health ../../../checks_compiled/
cp src/check_ceph_mds ../../../checks_compiled/
cp src/check_ceph_mgr ../../../checks_compiled/
cp src/check_ceph_mon ../../../checks_compiled/
cp src/check_ceph_osd ../../../checks_compiled/
cp src/check_ceph_rgw ../../../checks_compiled/
cp src/check_ceph_rgw_api ../../../checks_compiled/
