#!/bin/bash
set -e

# compile
cd src/

# copy binary
cp check_wmi_range.pl ../../../checks_compiled/check_wmi_range
