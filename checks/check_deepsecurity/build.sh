#!/bin/bash
set -e

# compile
cd src/

# copy binary
cp check_deepsecurity.pl ../../../checks_compiled/check_deepsecurity
