#!/bin/bash
set -e

# compile
cd src/

# copy binary
cp check_hwg-ste2.pl ../../../checks_compiled/check_hwg-ste2
