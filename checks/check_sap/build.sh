#!/bin/bash
set -e

# compile
cd src/
autoreconf -i
./configure --enable-static --disable-maintainer-mode --prefix=/usr --with-nagios-user=nagios --with-nagios-group=nagios
make

# copy binary
cp check_sap ../../../checks_compiled/check_sap
