#!/bin/bash
set -e

# compile
cd src/

# copy binary
cp check_disk_btrfs ../../../checks_compiled/check_disk_btrfs
