#!/bin/bash
set -e

# compile
cd src/

# copy binary
cp check_smseagle ../../../checks_compiled/check_smseagle
cp notify_smseagle ../../../checks_compiled/notify_smseagle
