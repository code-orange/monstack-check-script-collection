#!/bin/bash
set -e

# compile
cd src/

# copy binary
cp check_modbus ../../../checks_compiled/check_modbus
