#!/bin/bash
set -e

# compile
cd src/

# copy binary
cp check_gtp_v2.pl ../../../checks_compiled/check_gtp_v2
cp multiplexer.pl ../../../checks_compiled/multiplexer
