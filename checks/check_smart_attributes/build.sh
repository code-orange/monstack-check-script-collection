#!/bin/bash
set -e

# compile
cd src/

# copy binary
cp check_smart_attributes ../../../checks_compiled/check_smart_attributes
