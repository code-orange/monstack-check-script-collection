#!/bin/bash
set -e

# compile
cd src/

# copy binary
cp check_usbtlight.pl ../../../checks_compiled/check_usbtlight
