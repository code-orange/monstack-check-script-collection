#!/bin/bash
set -e

# compile
cd src/

# copy binary
cp check_smartvalues ../../../checks_compiled/check_smartvalues
