#!/usr/bin/env python3
from github import Github

github = Github()

# github_org = github.get_organization('NETWAYS')
github_org = github.get_user("lausser")
github_repos = github_org.get_repos()

git_urls = list()

for repository in github_repos:
    if "check" in repository.full_name:
        git_urls.append(repository.clone_url)

for clone_url in sorted(git_urls):
    clone_url = clone_url.replace(".git", "")
    module_name = clone_url.split("/")[-1]
    if module_name.startswith("check"):
        print(
            'curl -X POST "http://192.168.81.5:8080/job/check_commands_add_module/buildWithParameters?GIT_URL_MODULE='
            + clone_url
            + "&MODULE_NAME="
            + module_name
            + '"'
        )
